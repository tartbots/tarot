#/usr/bin/env python3
from ananas import reply, PineappleBot
import random
import os

class tarotBot(PineappleBot):
	def start(self):
		with open("tarot/cards.txt","r") as lol:
			self.cards = lol.read().split("\n")
		self.allcommands(True)
	def getcard(self):
		return random.choice(self.cards)
	@reply
	def say_this(self, status, user):
		self.mastodon.status_post("@"+user["acct"]+"\n"+self.getcard(),visibility=status["visibility"],in_reply_to_id=status["id"])
	def allcommands(self,toot):
		allcs = "Hi! I'm Tarot Bot. I will draw you random tarot cards when you toot me! They're not very pretty yet, but they will be soon! ❤️"
		if(toot):
			pass#self.mastodon.toot(allcs)
		else:
			return allcs
	#def start(self):
	#	self.mastodon.status_post("@weird_hell@cybre.space gmorning")
	#	#self.mastodon.toot("Good morning! Here are my commands:\n"+self.allcommands(False))
	def stop(self):
		self.mastodon.status_post("@weird_hell@cybre.space gnight",visibility="direct")
		#self.mastodon.toot("Good night.")
